# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 17:50:33 2017

@author: 212415648
"""

import numpy as np
from PIL import Image, ImageOps


def conv2d(x, k):
    """ Code adapted from C++ implementation of 2d convolution by Song Ho Ahn
    (http://www.songho.ca/dsp/convolution/convolution.html#separable_convolution) """
    kCenterX = int(k.shape[1] / 2)
    kCenterY = int(k.shape[0] / 2)
    out = np.zeros(x.shape)
    for row in range(0, x.shape[0]):
        for column in range(0, x.shape[1]):
            for kernel_row in range(0, k.shape[0]):
                mm = k.shape[0] - 1 - kernel_row
                for kernel_column in range(0, k.shape[1]):
                    nn = k.shape[1] - 1 - kernel_column
                    ii = row + (kernel_row - kCenterY)
                    jj = column + (kernel_column - kCenterX)
                    if 0 <= ii < x.shape[0] and 0 <= jj < x.shape[1]:
                        out[row, column] += x[ii, jj] * k[mm, nn]
    return out

# The following Harris corner detection was adapted from code by
# Kai Jiang, to remove any dependency with scipy. Code sourced
# from (http://www.kaij.org/blog/?p=89)


def gauss_derivatives(im, n, ny=None):
    """ returns x and y derivatives of an image using gaussian
        derivative filters of size n. The optional argument
        ny allows for a different size in the y direction."""

    gx, gy = gauss_derivative_kernels(n, sizey=ny)

    imx = conv2d(im, gx)
    imy = conv2d(im, gy)

    return imx,imy


def gauss_derivative_kernels(size, sizey=None):
    """ returns x and y derivatives of a 2D
        gauss kernel array for convolutions """
    size = int(size)
    if not sizey:
        sizey = size
    else:
        sizey = int(sizey)
    y, x = np.mgrid[-size:size+1, -sizey:sizey+1]

    #x and y derivatives of a 2D gaussian with standard dev half of size
    # (ignore scale factor)
    gx = - x * np.exp(-(x**2/float((0.5*size)**2)+y**2/float((0.5*sizey)**2)))
    gy = - y * np.exp(-(x**2/float((0.5*size)**2)+y**2/float((0.5*sizey)**2)))

    return gx,gy


def gauss_kernel(size, sizey = None):
    """ Returns a normalized 2D gauss kernel array for convolutions """
    size = int(size)
    if not sizey:
        sizey = size
    else:
        sizey = int(sizey)
    x, y = np.mgrid[-size:size+1, -sizey:sizey+1]
    g = np.exp(-(x**2/float(size)+y**2/float(sizey)))
    return g / g.sum()


def compute_harris_response(image):
    """ compute the Harris corner detector response function
        for each pixel in the image"""

    # derivatives
    imx, imy = gauss_derivatives(image, 3)

    # kernel for blurring
    gauss = gauss_kernel(3)

    # compute components of the structure tensor
    Wxx = conv2d(imx * imx, gauss)
    Wxy = conv2d(imx * imy, gauss)
    Wyy = conv2d(imy * imy, gauss)

    # determinant and trace
    Wdet = Wxx * Wyy - Wxy ** 2
    Wtr = Wxx + Wyy

    return Wdet / Wtr


def get_harris_points(harrisim, min_distance=10, threshold=0.1):
    """ return corners from a Harris response image
        min_distance is the minimum nbr of pixels separating
        corners and image boundary"""

    # find top corner candidates above a threshold
    corner_threshold = max(harrisim.ravel()) * threshold
    harrisim_t = (harrisim > corner_threshold) * 1

    # get coordinates of candidates
    candidates = harrisim_t.nonzero()
    coords = [(candidates[0][c], candidates[1][c]) for c in range(len(candidates[0]))]
    # ...and their values
    candidate_values = [harrisim[c[0]][c[1]] for c in coords]

    # sort candidates
    index = np.argsort(candidate_values)

    # store allowed point locations in array
    allowed_locations = np.zeros(harrisim.shape)
    allowed_locations[min_distance:-min_distance, min_distance:-min_distance] = 1

    # select the best points taking min_distance into account
    filtered_coords = []
    for i in index:
        if allowed_locations[coords[i][0]][coords[i][1]] == 1:
            filtered_coords.append(coords[i])
            allowed_locations[(coords[i][0] - min_distance):(coords[i][0] + min_distance),
            (coords[i][1] - min_distance):(coords[i][1] + min_distance)] = 0

    return filtered_coords


# End code adapted from Kai Jiang

def main(fig):
    bbox = np.subtract(ImageOps.invert(fig).getbbox(),(10,10,-10,-10))
    im = np.array(fig.crop(bbox))
    harrisim = compute_harris_response(im)
    filtered_coords = get_harris_points(harrisim)
    return filtered_coords
