# Make clustering to get number of corners
# independently of shape fill
def kmeans(X, k):
    L = []
    L1 = 0
    while len(np.unique(np.array(L))) != k:
        # Initialize
        C = X[:, int(1 + np.round(random.random() * (X.shape[1] - 1)))]
        L = np.ones((1, X.shape[1]))
        for i in range(2, k + 1):
            if i == 2:
                D = X - np.outer(C, L)
            else:
                CL = np.zeros(X.shape)
                for ii in range(1, i):
                    if str(type(C)) != "<class 'numpy.matrixlib.defmatrix.matrix'>":
                        CL[:, np.array(L == ii)[0]] = np.tile(np.matrix(C[:, ii - 1]).transpose(),
                                                              CL[:, np.array(L == ii)[0]].shape[1])
                    else:
                        CL[:, np.array(L == ii)[0]] = np.tile(np.matrix(C[:, ii - 1]),
                                                              CL[:, np.array(L == ii)[0]].shape[1])
                D = X - CL
            D = np.cumsum(np.sqrt(np.sum(np.multiply(D, D), axis=0)))
            if D[-1] == 0:
                C[:, i:k] = X[:, np.ones((1, k - i + 1)).astype(int)]
                break
            if i == 2:
                C = np.stack((C, X[:, np.argmax(random.random() < D / D[-1])]))
                subs1 = 2 * np.real(np.dot(C, X))
                subs2 = np.tile(np.matrix(np.sum(np.multiply(np.transpose(C), np.transpose(C)), axis=0)).transpose(),
                                X.shape[1])
            else:
                C = np.column_stack((np.matrix(C), np.matrix(X[:, np.argmax(random.random() < D / D[-1])]).transpose()))
                subs1 = 2 * np.real(np.dot(C.transpose(), X))
                subs2 = np.tile(np.matrix(np.sum(np.multiply(C, C), axis=0)).transpose(), X.shape[1])
            L = np.argmax(subs1 - subs2, axis=0) + 1
        while (L != L1).any():
            L1 = L
            for i in range(0, k):
                if i <= C.shape[1] - 1:
                    if k <= 2:
                        C[:, i] = (
                        np.sum(X[:, np.array(L == i + 1)[0, :]], axis=1) / np.sum(np.array(L == i + 1))).transpose()
                    else:
                        C[:, i] = np.matrix(
                            np.sum(X[:, np.array(L == i + 1)[0, :]], axis=1) / np.sum(np.array(L == i + 1))).transpose()
                else:
                    C = np.column_stack(
                        (C, np.sum(X[:, np.array(L == i + 1)[0, :]], axis=1) / np.sum(np.array(L == i + 1))))
            subs1 = 2 * np.real(np.dot(C.transpose(), X))
            subs2 = np.tile(np.matrix(np.sum(np.multiply(C, C), axis=0)).transpose(), X.shape[1])
            L = np.argmax(subs1 - subs2, axis=0) + 1
    return L, C


def silhouette(X, clust):
    idx = clust
    idx.shape = (len(idx), 1)
    n = idx.size
    k = len(np.unique(np.array(idx)))
    mbrs = np.tile(np.arange(1, k + 1), (n, 1)) == np.tile(clust, (1, k))
    count = np.bincount(np.reshape(np.array(clust), np.array(clust).size))[1:k + 1]
    myinf = float('Inf')
    avgDWithin = np.tile(myinf, (n, 1))
    avgDBetween = np.tile(myinf, (n, k))
    for j in range(0, n):
        distj = np.sum((X - X[j, :]) ** 2, axis=1)
        distj.shape = (len(distj), 1)
        for i in range(0, k):
            if i + 1 == idx[j]:
                avgDWithin[j] = np.sum(distj[mbrs[:, i]]) / np.max((count[i] - 1, 1))
            else:
                avgDBetween[j, i] = np.sum(distj[mbrs[:, i]]) / count[i]
    minavgDBetween = np.min(avgDBetween, axis=1)
    minavgDBetween.shape = (len(minavgDBetween), 1)
    silh = (minavgDBetween - avgDWithin) / np.max((avgDWithin, minavgDBetween), axis=0)
    return np.mean(silh)
