# Your Agent for solving Raven's Progressive Matrices. You MUST modify this file.
#
# You may also create and submit new files in addition to modifying this file.
#
# Make sure your file retains methods with the signatures:
# def __init__(self)
# def Solve(self,problem)
#
# These methods will be necessary for the project's main method to run.

# Install Pillow and uncomment this line to access image processing.
from PIL import Image, ImageOps, ImageMath, ImageChops
import numpy as np
import math

# Implements 8-connectivity connected component labeling
#
# Algorithm obtained from "Optimizing Two-Pass Connected-Component Labeling
# by Kesheng Wu, Ekow Otoo, and Kenji Suzuki
import cclabel


########################################################################
# Image similarity module
# Two approaches to understand the difference (or similarity)
# of two provided images within a filesystem, from a pixel perspective
# (c) 2015 Tony DiLoreto
########################################################################


def get_image_pixel_similarity(image1, image2):
    # First thing to do is resize image2 to be image1's dimensions
    image2 = image2.resize(image1.size, Image.BILINEAR)

    # each of these are lists of (r,g,b) values
    image1_pixels = list(image1.getdata())
    image2_pixels = list(image2.getdata())

    # now need to compare the r, g, b values for each image2_pixels

    # initialize vars
    i = 0
    tot_img_diff = 0
    diff_pixels = 0

    for pix1 in image1_pixels:
        pix2 = image2_pixels[i]

        r_diff = abs(pix1[0] - pix2[0])
        g_diff = abs(pix1[1] - pix2[1])
        b_diff = abs(pix1[2] - pix2[2])

        tot_pix_diff = (r_diff + g_diff + b_diff)

        if tot_pix_diff != 0:
            # print("comparing: " , pix1 , " to " , pix2)
            diff_pixels += 1

        i += 1

        # keep a running total of the difference of each pixel triplet
        tot_img_diff += tot_pix_diff

    # the greatest difference will be 765 * image1.size

    # now calculate our proprietary 'similarity score'
    # similarity = 1 - difference %
    # difference % = tot_img_diff / (image1_size * 255 * 3)
    # where the denominator is the maximum difference

    # print(i)
    # print(tot_img_diff)

    tot_pix = image1.size[0] * image1.size[1]
    hues = 255
    channels = 3

    img_diff = float(diff_pixels / tot_pix)
    img_sim = 1 - img_diff

    return img_sim


def nrmse(im1, im2):
    return (np.array(im1) == np.array(im2)).sum() / np.array(im1).size


def transform22(frame1, frame2):
    transform = np.empty([np.max([frame1.objects.shape[0], frame2.objects.shape[0]]), 1], dtype=object)
    for objIdx in range(0, np.max([frame1.objects.shape[0], frame2.objects.shape[0]])):
        if objIdx <= frame1.objects.shape[0] - 1 & objIdx <= frame2.objects.shape[0] - 1:
            subsArray = np.array([frame1.objects[objIdx, :], frame2.objects[objIdx, :]])[:, 1:frame1.objects.shape[1]]
            diffArray = subsArray[0, :] == subsArray[1, :]
            if subsArray[0, 0] == 'empty':
                transform[objIdx] = 'added-from-empty'
                continue
            if all(diffArray):
                transform[objIdx] = 'unchanged'
            if ~diffArray[3]:
                transform[objIdx] = 'rotated'
            if ~diffArray[2]:
                transform[objIdx] = 'fill-change'
            if ~diffArray[1]:
                if ((frame1.objects[0, 1] == 'rectangle' or frame1.objects[0, 1] == 'square') and
                        (frame2.objects[0, 1] == 'rectangle' or frame2.objects[0, 1] == 'square')):
                    transform[objIdx] = 'rectangle-resize'
                else:
                    if (subsArray[0, 1] - subsArray[1, 1]) < 1:
                        transform[objIdx] = 'enlarged'
                    if (subsArray[0, 1] - subsArray[1, 1]) > 1:
                        transform[objIdx] = 'shrunk'
            if ~diffArray[4] or ~diffArray[5]:
                transform[objIdx] = 'rectangle-resize'
        if objIdx > frame1.objects.shape[0] - 1:
            transform[objIdx] = 'added'
        if objIdx > frame2.objects.shape[0] - 1:
            transform[objIdx] = 'deleted'
    return transform


def loadImage(problem, figureName):
    figure = problem.figures[figureName]
    figureImage = Image.open(figure.visualFilename).convert('L')
    return figureImage


def diffImage22(problem, diff):
    responseAcc = np.zeros((6, 1))
    for responseIdx in range(1, 7):
        responseFig = loadImage(problem, str(responseIdx))
        responseAcc[responseIdx - 1] = ((np.array(diff) == np.array(responseFig)).sum() / np.array(responseFig).size)
    solution = (np.argmax(responseAcc) + 1)
    return solution


def fillChange22(frame1, frame2, frame3, frames):
    solution = -1
    if ((all(frame1.objects[:, 3] == 'no') & all(frame2.objects[:, 3] == 'yes')) | (
                all(frame1.objects[:, 3] == 'yes') & all(frame2.objects[:, 3] == 'no'))):
        for responseIdx in range(1, 7):
            currentObjects = next((x for x in frames if x.name == str(responseIdx)), None).objects
            if all(currentObjects[0][1:3] == frame3.objects[0][1:3]) & (currentObjects[0][3] != frame3.objects[0][3]):
                solution = responseIdx
    return solution


def rotate22(problem, fig, switch):
    responseAcc = np.zeros((6, 1))
    if switch == 'mirror':
        for responseIdx in range(1, 7):
            responseFig = loadImage(problem, str(responseIdx))
            responseAcc[responseIdx - 1] = (np.array(ImageOps.mirror(fig)) == np.array(responseFig)).sum() / np.array(
                responseFig).size
    if switch == 'flip':
        for responseIdx in range(1, 7):
            responseFig = loadImage(problem, str(responseIdx))
            responseAcc[responseIdx - 1] = (np.array(ImageOps.flip(fig)) == np.array(responseFig)).sum() / np.array(
                responseFig).size
    solution = (np.argmax(responseAcc) + 1)
    acc = np.max(responseAcc)
    return solution, acc


def convertSize(size):
    if size == 'tiny':
        size = 0
    elif size == 'very small':
        size = 1
    elif size == 'small':
        size = 2
    elif size == 'medium':
        size = 3
    elif size == 'large':
        size = 4
    elif size == 'very large':
        size = 5
    elif size == 'huge':
        size = 6
    return size


def getFigDim(fig):
    figCrop = cropImage(fig)
    width = figCrop.shape[0]
    height = figCrop.shape[1]
    return width, height


def imageLogic1(fig1, fig2):
    return ImageMath.eval('a&b', a=ImageOps.invert(ImageMath.eval('(a&~b)', a=fig1, b=fig2).convert('L')),
                          b=ImageOps.mirror(ImageOps.invert(ImageMath.eval('(a&~b)', a=fig1, b=fig2).convert('L'))))


def cropImage(fig):
    fig_crop = np.array(fig)
    fig_crop = fig_crop[:, (fig_crop < 255).any(axis=0)]
    fig_crop = fig_crop[(fig_crop < 255).any(axis=1), :]
    return fig_crop


def rmsdiff(im1, im2):
    "Calculate the root-mean-square difference between two images"
    diff = ImageChops.difference(im1, im2)
    h = diff.histogram()
    sq = (value * ((idx % 256) ** 2) for idx, value in enumerate(h))
    sum_of_squares = sum(sq)
    rms = math.sqrt(sum_of_squares / float(im1.size[0] * im1.size[1]))
    return rms


def isfilled(blob):
    # Get center coordinate
    coords = np.where(blob != 255)
    centerX = int(np.mean((np.min(coords[0]), np.max(coords[0]))))
    centerY = int(np.mean((np.min(coords[1]), np.max(coords[1]))))
    center = blob[centerX, centerY]
    if center == 0:
        return True
    else:
        return False
    pass


class Frame(object):
    name = ""
    objects = ""

    def __init__(self, name, objects):
        self.name = name
        self.objects = objects


class Agent:
    # The default constructor for your Agent. Make sure to execute any
    # processing necessary before your Agent starts solving problems here.
    #
    # Do not add any variables to this signature; they will not be used by
    # main().
    def __init__(self):
        pass

    # The primary method for solving incoming Raven's Progressive Matrices.
    # For each problem, your Agent's Solve() method will be called. At the
    # conclusion of Solve(), your Agent should return an int representing its
    # answer to the question: 1, 2, 3, 4, 5, or 6. Strings of these ints
    # are also the Names of the individual RavensFigures, obtained through
    # RavensFigure.getName(). Return a negative number to skip a problem.
    #
    # Make sure to return your answer *as an integer* at the end of Solve().
    # Returning your answer as a string may cause your program to crash.

    def Solve(self, problem):
        # Order the figure names, by letter-named figures first, which correspond to the
        # problem, then by numbered figures, which correspond to the possible solutions
        # to the problem

        solution = -1

        # If problem has verbal representation retrieve the attributes and then build the
        # semantic network
        if problem.hasVerbal:
            sortedNames = sorted(problem.figures, key=lambda name: (name[0].isdigit(), name))
            attrNames = ['shape', 'size', 'fill', 'angle', 'width', 'height']
            frames = [Frame(name='', objects='') for i in range(0, len(sortedNames))]

            for figureIdx, figureName in enumerate(sortedNames):
                thisFigure = problem.figures[figureName]
                frames[figureIdx].name = figureName
                featureMatrix = np.empty([len(thisFigure.objects), len(attrNames) + 1], dtype=object)
                for objectIdx, objectName in enumerate(thisFigure.objects):
                    thisObject = thisFigure.objects[objectName]
                    featureMatrix[objectIdx, 0] = objectIdx
                    for attrIdx, attr in enumerate(attrNames):
                        if attr in thisObject.attributes:
                            if attr == 'size':
                                thisObject.attributes[attr] = convertSize(thisObject.attributes[attr])
                            featureMatrix[objectIdx, attrIdx + 1] = thisObject.attributes[attr]
                frames[figureIdx].objects = featureMatrix[featureMatrix[:, 0].argsort()]
                if len(thisFigure.objects) == 0:
                    featureMatrix = np.empty([1, len(attrNames) + 1], dtype=object)
                    featureMatrix[0,] = np.array(['a', 'empty', None, None, None, None, None])
                    frames[figureIdx].objects = featureMatrix

                    # For 2x2 problems process frames A & B, as well as A & C to determine
                    # the transformation matrix

        if problem.problemType == '2x2' and problem.hasVerbal:
            frameA = (next((x for x in frames if x.name == 'A'), None))
            figA = loadImage(problem, 'A')

            frameB = (next((x for x in frames if x.name == 'B'), None))
            figB = loadImage(problem, 'B')

            frameC = (next((x for x in frames if x.name == 'C'), None))
            figC = loadImage(problem, 'C')

            # Get transformation between A & B
            transformAB = transform22(frameA, frameB)
            transformAC = transform22(frameA, frameC)

            if any(transformAB == 'rotated'):
                acc1 = 0
                acc2 = 0
                solution1 = -1
                solution2 = -1
                if (np.array(ImageOps.mirror(figA)) == np.array(figB)).sum() / np.array(figB).size >= 0.95:
                    solution1, acc1 = rotate22(problem, figC, 'mirror')
                if (np.array(ImageOps.flip(figA)) == np.array(figB)).sum() / np.array(figB).size >= 0.95:
                    solution2, acc2 = rotate22(problem, figC, 'flip')
                solution = solution1 if acc1 > acc2 else solution2

            if any(transformAC == 'rotated'):
                acc1 = 0
                acc2 = 0
                solution1 = -1
                solution2 = -1
                if (np.array(ImageOps.mirror(figA)) == np.array(figC)).sum() / np.array(figC).size >= 0.95:
                    solution1, acc1 = rotate22(problem, figB, 'mirror')
                if (np.array(ImageOps.flip(figA)) == np.array(figC)).sum() / np.array(figC).size >= 0.95:
                    solution2, acc2 = rotate22(problem, figB, 'flip')
                solution = solution1 if acc1 > acc2 else solution2

            if rmsdiff(figA, figC) < 35:
                responseAcc = np.zeros((6, 1))
                for responseIdx in range(1, 7):
                    responseFig = loadImage(problem, str(responseIdx))
                    responseAcc[responseIdx - 1] = rmsdiff(figB.convert('P'), responseFig.convert('P'))
                solution = (np.argmin(responseAcc) + 1)

            if rmsdiff(figA, figC) == 0 and rmsdiff(figA, figB) == 0:
                responseAcc = np.zeros((6, 1))
                for responseIdx in range(1, 7):
                    responseFig = loadImage(problem, str(responseIdx))
                    responseAcc[responseIdx - 1] = rmsdiff(figA.convert('P'), responseFig.convert('P'))
                solution = (np.argmin(responseAcc) + 1)

            if all(transformAB == 'fill-change') | all(transformAC == 'fill-change'):
                if all(transformAB == 'fill-change'):
                    solution = fillChange22(frameA, frameB, frameC, frames)
                if all(transformAC == 'fill-change'):
                    solution = fillChange22(frameA, frameC, frameB, frames)

            if any(transformAC == 'added') | any(transformAC == 'deleted'):
                diff = ImageMath.eval("b-(a-c)", a=figA, b=figB, c=figC)
                solution = diffImage22(problem, diff)

            if any(transformAB == 'added') | any(transformAB == 'deleted'):
                diff = ImageMath.eval("c-(a-b)", a=figA, b=figB, c=figC)
                solution = diffImage22(problem, diff)

        if problem.problemType == '3x3':

            global accuracy
            accuracy = 0

            figA = loadImage(problem, 'A')
            figB = loadImage(problem, 'B')
            figC = loadImage(problem, 'C')
            figD = loadImage(problem, 'D')
            figE = loadImage(problem, 'E')
            figF = loadImage(problem, 'F')
            figG = loadImage(problem, 'G')
            figH = loadImage(problem, 'H')

            if problem.hasVerbal:

                frameA = (next((x for x in frames if x.name == 'A'), None))
                frameB = (next((x for x in frames if x.name == 'B'), None))
                frameC = (next((x for x in frames if x.name == 'C'), None))
                frameD = (next((x for x in frames if x.name == 'D'), None))
                frameE = (next((x for x in frames if x.name == 'E'), None))
                frameF = (next((x for x in frames if x.name == 'F'), None))
                frameG = (next((x for x in frames if x.name == 'G'), None))
                frameH = (next((x for x in frames if x.name == 'H'), None))

                transformAB = transform22(frameA, frameB)
                transformBC = transform22(frameB, frameC)
                transformAD = transform22(frameA, frameD)
                transformDG = transform22(frameD, frameG)
                transformBE = transform22(frameB, frameE)
                transformEH = transform22(frameE, frameH)
                transformCF = transform22(frameC, frameF)
                transformDE = transform22(frameD, frameE)
                transformEF = transform22(frameE, frameF)
                transformGH = transform22(frameG, frameH)

                if (np.all(np.concatenate((frameA.objects[:, 1],
                                           frameB.objects[:, 1],
                                           frameC.objects[:, 1],
                                           frameD.objects[:, 1],
                                           frameG.objects[:, 1]), axis=0) == frameA.objects[0, 1])):
                    if (np.all(np.logical_or(transformAB == 'unchanged', transformAB == 'added'))
                            & np.all(np.logical_or(transformBC == 'unchanged', transformBC == 'added'))
                            & np.all(np.logical_or(transformAD == 'unchanged', transformAD == 'added'))
                            & np.all(np.logical_or(transformDG == 'unchanged', transformDG == 'added'))):
                        delta = frameH.objects.shape[0] - frameG.objects.shape[0]
                        for responseIdx in range(1, 9):
                            responseFrame = (next((x for x in frames if x.name == str(responseIdx)), None))
                            if np.all(responseFrame.objects[:, 1] == frameG.objects[0, 1]):
                                if responseFrame.objects.shape[0] - frameH.objects.shape[0] == delta:
                                    return responseIdx

                if (np.all(np.logical_or(transformAB == 'unchanged', transformAB == 'rectangle-resize'))
                        & np.all(np.logical_or(transformBC == 'unchanged', transformBC == 'rectangle-resize'))
                        & np.all(np.logical_or(transformAD == 'unchanged', transformAD == 'rectangle-resize'))
                        & np.all(np.logical_or(transformDG == 'unchanged', transformDG == 'rectangle-resize'))):
                    if (np.all(frameA.objects[:, 1] == frameB.objects[:, 1])
                            & np.all(frameB.objects[:, 1] == frameC.objects[:, 1])
                            & np.all(frameA.objects[:, 1] == frameD.objects[:, 1])
                            & np.all(frameD.objects[:, 1] == frameG.objects[:, 1])):
                        if (transformAB == transformBC).all() & (transformBC == transformAD).all() & (
                                    transformAD == transformDG).all():
                            if (((frameB.objects[:, 2] - frameA.objects[:, 2]) == (
                                        frameE.objects[:, 2] - frameD.objects[:, 2])).all() and
                                    ((frameC.objects[:, 2] - frameB.objects[:, 2]) == (
                                                frameF.objects[:, 2] - frameE.objects[:, 2])).all() and
                                    ((frameB.objects[:, 2] - frameA.objects[:, 2]) == (
                                                frameH.objects[:, 2] - frameG.objects[:, 2])).all()):
                                for responseIdx in range(1, 9):
                                    responseTransform = transform22(frameH, (
                                        next((x for x in frames if x.name == str(responseIdx)), None)))
                                    responseFrame = (next((x for x in frames if x.name == str(responseIdx)), None))
                                    if responseTransform.size == transformGH.size:
                                        if ((responseTransform == transformGH).all() and (
                                                    (frameH.objects[:, 2] - frameG.objects[:, 2]) == (
                                                            responseFrame.objects[:, 2] - frameH.objects[:, 2])).all()):
                                            return responseIdx

            figAw, figAl = getFigDim(figA)
            figBw, figBl = getFigDim(figB)
            figCw, figCl = getFigDim(figC)
            figDw, figDl = getFigDim(figD)
            figEw, figEl = getFigDim(figE)
            figFw, figFl = getFigDim(figF)
            # figGw, figGl = getFigDim(figG)
            figHw, figHl = getFigDim(figH)

            figA_crop = cropImage(figA)
            figB_crop = cropImage(figB)
            figC_crop = cropImage(figC)
            figD_crop = cropImage(figD)
            figE_crop = cropImage(figE)
            figF_crop = cropImage(figF)
            figG_crop = cropImage(figG)
            figH_crop = cropImage(figH)

            diffAB = ImageMath.eval('a-b', a=figA, b=figB)
            diffDE = ImageMath.eval('d-e', d=figD, e=figE)
            diffGH = ImageMath.eval('g-h', g=figG, h=figH)
            diffBC = ImageMath.eval('b-c', b=figB, c=figC)
            diffEF = ImageMath.eval('e-f', e=figE, f=figF)

            if figAw > 0 and figBw > 0 and figEw > 0 and figDw > 0 and figAl > 0 and figBl > 0 and figEl > 0 and figDl > 0:
                if rmsdiff(Image.fromarray(figA_crop), Image.fromarray(figC_crop)) < 35:
                    if ((1.0 >= ((1.0 * figBw / 1.0 * figAw) / (1.0 * figEw / 1.0 * figDw)) >= 0.90) and
                            (1.0 >= ((1.0 * figCw / 1.0 * figBw) / (1.0 * figFw / 1.0 * figEw)) >= 0.90) and
                            (1.0 >= ((1.0 * figBl / 1.0 * figAl) / (1.0 * figEl / 1.0 * figDl)) >= 0.90) and
                            (1.0 >= ((1.0 * figCl / 1.0 * figBl) / (1.0 * figFl / 1.0 * figEl)) >= 0.90)):
                        for responseIdx in range(1, 9):
                            responseFig = loadImage(problem, str(responseIdx))
                            rFw, rFl = getFigDim(responseFig)
                            if ((1.0 >= ((1.0 * rFw / 1.0 * figHw) / (1.0 * figFw / 1.0 * figEw)) > 0.90) and
                                    (1.0 >= ((1.0 * rFl / 1.0 * figHl) / (1.0 * figFl / 1.0 * figEl)) > 0.90)):
                                return responseIdx

            if (rmsdiff(diffAB.convert('L'), diffDE.convert('L')) < 35 and
                        rmsdiff(diffDE.convert('L'), diffGH.convert('L')) < 35 and
                        rmsdiff(diffBC.convert('L'), diffEF.convert('L')) < 35):
                responseAcc = np.zeros((8, 1))
                generatedResponse = ImageMath.eval('h-delta', h=figH, delta=diffEF)
                for responseIdx in range(1, 9):
                    responseFig = loadImage(problem, str(responseIdx))
                    responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                              responseFig.convert('RGB'))
                if max(responseAcc) > accuracy:
                    solution = (np.argmax(responseAcc) + 1)
                    accuracy = max(responseAcc)

            if (rmsdiff(ImageOps.mirror(figA), figC) < 35 and
                        rmsdiff(ImageOps.mirror(figD), figF) < 35):
                responseAcc = np.zeros((8, 1))
                generatedResponse = ImageOps.mirror(figG)
                for responseIdx in range(1, 9):
                    responseFig = loadImage(problem, str(responseIdx))
                    responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                              responseFig.convert('RGB'))
                if max(responseAcc) > accuracy:
                    solution = (np.argmax(responseAcc) + 1)
                    accuracy = max(responseAcc)

            if (rmsdiff(ImageOps.flip(figA), figG) < 35 and
                        rmsdiff(ImageOps.flip(figB), figH) < 35):
                responseAcc = np.zeros((8, 1))
                generatedResponse = ImageOps.flip(figC)
                for responseIdx in range(1, 9):
                    responseFig = loadImage(problem, str(responseIdx))
                    responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                              responseFig.convert('RGB'))
                if max(responseAcc) > accuracy:
                    solution = (np.argmax(responseAcc) + 1)
                    accuracy = max(responseAcc)

            if (rmsdiff(ImageOps.flip(figA), figC) < 35 and
                        rmsdiff(ImageOps.flip(figD), figF) < 35):
                responseAcc = np.zeros((8, 1))
                generatedResponse = ImageOps.flip(figG)
                for responseIdx in range(1, 9):
                    responseFig = loadImage(problem, str(responseIdx))
                    responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                              responseFig.convert('RGB'))
                if max(responseAcc) > accuracy:
                    solution = (np.argmax(responseAcc) + 1)
                    accuracy = max(responseAcc)

            if (rmsdiff(ImageOps.mirror(figA), figG) < 35 and
                        rmsdiff(ImageOps.mirror(figB), figH) < 35):
                responseAcc = np.zeros((8, 1))
                generatedResponse = ImageOps.mirror(figC)
                for responseIdx in range(1, 9):
                    responseFig = loadImage(problem, str(responseIdx))
                    responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                              responseFig.convert('RGB'))
                if max(responseAcc) > accuracy:
                    solution = (np.argmax(responseAcc) + 1)
                    accuracy = max(responseAcc)

            if ((np.array(imageLogic1(figA, figB)) == np.array(figC)).sum() / np.array(figC).size > 0.95 and
                            (np.array(imageLogic1(figD, figE)) == np.array(figF)).sum() / np.array(figF).size > 0.95):
                responseAcc = np.zeros((8, 1))
                generatedResponse = imageLogic1(figG, figH)
                for responseIdx in range(1, 9):
                    responseFig = loadImage(problem, str(responseIdx))
                    responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                              responseFig.convert('RGB'))
                if max(responseAcc) > accuracy:
                    solution = (np.argmax(responseAcc) + 1)
                    accuracy = max(responseAcc)

            if figA_crop.size > 0:
                offsetA = ImageChops.offset(Image.fromarray(figA_crop), int(Image.fromarray(figA_crop).size[0] / 2), 0)
                offsetA = offsetA.resize(list(reversed(np.array(figC_crop).shape)))
                accA = (0.90 < (np.array(figC_crop) == np.array(offsetA)).sum() / np.array(figC_crop).size)
                offsetD = ImageChops.offset(Image.fromarray(figD_crop), int(Image.fromarray(figD_crop).size[0] / 2), 0)
                offsetD = offsetD.resize(list(reversed(np.array(figF_crop).shape)))
                accB = (0.90 < (np.array(figF_crop) == np.array(offsetD)).sum() / np.array(figF_crop).size)
                if accA and accB:
                    responseAcc = np.zeros((8, 1))
                    offsetG = ImageChops.offset(Image.fromarray(figG_crop), int(Image.fromarray(figG_crop).size[0] / 2),
                                                0)
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        responseCrop = cropImage(responseFig)
                        generatedResponse = offsetG.resize(list(reversed(np.array(responseCrop).shape)))
                        responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                                  Image.fromarray(responseCrop).convert(
                                                                                      'RGB'))
                    if max(responseAcc) > accuracy:
                        solution = (np.argmax(responseAcc) + 1)
                        accuracy = max(responseAcc)

            if figA_crop.size > 0:
                horzStackA = Image.fromarray(np.hstack((figA_crop, figA_crop)))
                horzStackA = horzStackA.resize(list(reversed(np.array(figC_crop).shape)))
                accHorzA = (0.90 < (np.array(horzStackA) == np.array(figC_crop)).sum() / np.array(figC_crop).size)
                horzStackD = Image.fromarray(np.hstack((figD_crop, figD_crop)))
                horzStackD = horzStackD.resize(list(reversed(np.array(figF_crop).shape)))
                accHorzD = (0.90 < (np.array(horzStackD) == np.array(figF_crop)).sum() / np.array(figF_crop).size)
                vertStackA = Image.fromarray(np.vstack((figA_crop, figA_crop)))
                vertStackA = vertStackA.resize(list(reversed(np.array(figG_crop).shape)))
                accVertA = (0.90 < (np.array(vertStackA) == np.array(figG_crop)).sum() / np.array(figG_crop).size)
                vertStackB = Image.fromarray(np.vstack((figB_crop, figB_crop)))
                vertStackB = vertStackB.resize(list(reversed(np.array(figH_crop).shape)))
                accVertB = (0.90 < (np.array(vertStackB) == np.array(figH_crop)).sum() / np.array(figH_crop).size)
                if accVertA and accVertB:
                    responseAcc = np.zeros((8, 1))
                    vertStackC = Image.fromarray(np.vstack((figC_crop, figC_crop)))
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        responseCrop = cropImage(responseFig)
                        generatedResponse = vertStackC.resize(list(reversed(np.array(responseCrop).shape)))
                        responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                                  Image.fromarray(responseCrop).convert(
                                                                                      'RGB'))
                    if max(responseAcc) > accuracy:
                        solution = (np.argmax(responseAcc) + 1)
                        accuracy = max(responseAcc)
                if accHorzA and accHorzD:
                    responseAcc = np.zeros((8, 1))
                    horzStackG = Image.fromarray(np.hstack((figG_crop, figG_crop)))
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        responseCrop = cropImage(responseFig)
                        generatedResponse = horzStackG.resize(list(reversed(np.array(responseCrop).shape)))
                        responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                                  Image.fromarray(responseCrop).convert(
                                                                                      'RGB'))
                    if max(responseAcc) > accuracy:
                        solution = (np.argmax(responseAcc) + 1)
                        accuracy = max(responseAcc)

            if figA.getbbox() is not None and figE.getbbox() is not None:
                # Check if the two elements in the diagonal are the same
                figA_cropped = figA.crop(ImageOps.invert(figA).getbbox())
                figE_cropped = figE.crop(ImageOps.invert(figE).getbbox())
                if rmsdiff(figA_cropped, figE_cropped) < 35:
                    responseAcc = np.zeros((8, 1))
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        responseCrop = cropImage(responseFig)
                        responseAcc[responseIdx - 1] = get_image_pixel_similarity(
                            Image.fromarray(figA_crop).convert('RGB'), Image.fromarray(responseCrop).convert('RGB'))
                    if max(responseAcc) > accuracy:
                        solution = (np.argmax(responseAcc) + 1)
                        accuracy = max(responseAcc)

                # Check if each consecutive image is having its outer edge removed on the cropped image
                if ImageOps.invert(figA_cropped).getbbox() is not None:
                    figA_cropped = figA_cropped.crop(
                        np.subtract(ImageOps.invert(figA_cropped).getbbox(), (-6, -6, 6, 6)))
                    figA_cropped = figA_cropped.crop(ImageOps.invert(figA_cropped).getbbox())
                    if rmsdiff(figA_cropped, figE_cropped) < 35:
                        responseAcc = np.zeros((8, 1))
                        generatedResponse = Image.fromarray(cropImage(ImageOps.crop(Image.fromarray(figE_crop), 6)))
                        for responseIdx in range(1, 9):
                            responseFig = loadImage(problem, str(responseIdx))
                            responseCrop = cropImage(responseFig)
                            responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                                      Image.fromarray(
                                                                                          responseCrop).convert('RGB'))
                        return np.argmax(responseAcc) + 1

            figA_index, figA_ncenter, figA_nfigures = cclabel.main(problem.figures['A'].visualFilename)
            figE_index, figE_ncenter, figE_nfigures = cclabel.main(problem.figures['E'].visualFilename)

            figA_outer = np.array(figA)
            figA_outer[figA_index == figA_ncenter] = 255
            figE_outer = np.array(figE)
            figE_outer[figE_index == figE_ncenter] = 255

            figA_center = np.array(figA)
            figA_center[figA_index != figA_ncenter] = 255
            figE_center = np.array(figE)
            figE_center[figE_index != figE_ncenter] = 255

            # Check if the center is changing but the outer figures are remaining similar
            if figA_nfigures > 1 and figE_nfigures > 1:
                if rmsdiff(Image.fromarray(figA_outer), Image.fromarray(figE_outer)) < 35 \
                        < rmsdiff(Image.fromarray(figA_center), Image.fromarray(figE_center)):
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        response_index, response_ncenter, response_nfigures = \
                            cclabel.main(problem.figures[str(responseIdx)].visualFilename)
                        response_outer = np.array(responseFig)
                        response_outer[response_index == response_ncenter] = 255
                        response_center = np.array(responseFig)
                        response_center[response_index != response_ncenter] = 255
                        if response_nfigures > 1 and rmsdiff(Image.fromarray(figE_outer),
                                                             Image.fromarray(response_outer)) < 35 \
                                < rmsdiff(Image.fromarray(figE_center), Image.fromarray(response_center)) and \
                                        rmsdiff(Image.fromarray(figA_center), Image.fromarray(response_center)) > 35:
                            return responseIdx

            if figA_nfigures == figE_nfigures:
                subfigA = Image.fromarray(np.array((figA_index == 1) * 255.0)).convert('L')
                subfigA = ImageOps.invert(subfigA.crop(np.subtract(subfigA.getbbox(), (10, 10, -10, -10)))).convert(
                    'RGB')
                subfigA_simm = np.zeros((figA_nfigures - 1, 1))
                for i in range(0, figA_nfigures - 1):
                    subfigA_i = Image.fromarray(np.array((figA_index == i + 2) * 255.0)).convert('L')
                    subfigA_i = ImageOps.invert(
                        subfigA_i.crop(np.subtract(subfigA_i.getbbox(), (10, 10, -10, -10)))).convert('RGB')
                    subfigA_simm[i] = get_image_pixel_similarity(subfigA_i, subfigA)
                subfigE = Image.fromarray(np.array((figE_index == 1) * 255.0)).convert('L')
                subfigE = ImageOps.invert(subfigE.crop(np.subtract(subfigE.getbbox(), (10, 10, -10, -10)))).convert(
                    'RGB')
                subfigE_simm = np.zeros((figE_nfigures - 1, 1))
                for i in range(0, figE_nfigures - 1):
                    subfigE_i = Image.fromarray(np.array((figE_index == i + 2) * 255.0)).convert('L')
                    subfigE_i = ImageOps.invert(
                        subfigE_i.crop(np.subtract(subfigE_i.getbbox(), (10, 10, -10, -10)))).convert('RGB')
                    subfigE_simm[i] = get_image_pixel_similarity(subfigE_i, subfigE)
                if np.mean(subfigA_simm) > 0.90 and np.mean(subfigE_simm) > 0.90:
                    delta = figE_nfigures - figA_nfigures
                    responseAcc = np.zeros((8, 1))
                    for responseIdx in range(1, 9):
                        responseAcc[responseIdx - 1] = 1
                        response_index, response_ncenter, response_nfigures = \
                            cclabel.main(problem.figures[str(responseIdx)].visualFilename)
                        if response_nfigures == figE_nfigures + delta:
                            subfigResponse = Image.fromarray(np.array((response_index == 1) * 255.0)).convert('L')
                            subfigResponse = ImageOps.invert(
                                subfigResponse.crop(np.subtract(subfigResponse.getbbox(), (10, 10, -10, -10)))).convert(
                                'RGB')
                            subfigResponse_simm = np.zeros((response_nfigures - 1, 1))
                            for i in range(0, response_nfigures - 1):
                                subfigResponse_i = Image.fromarray(np.array((response_index == i + 2) * 255.0)).convert(
                                    'L')
                                subfigResponse_i = ImageOps.invert(subfigResponse_i.crop(
                                    np.subtract(subfigResponse_i.getbbox(), (10, 10, -10, -10)))).convert('RGB')
                                subfigResponse_simm[i] = get_image_pixel_similarity(subfigResponse_i, subfigResponse)
                            if (subfigResponse_simm > 0.90).all():
                                responseAcc[responseIdx - 1] = np.mean((get_image_pixel_similarity(subfigResponse,
                                                                                                   subfigE),
                                                                        get_image_pixel_similarity(subfigResponse,
                                                                                                   subfigA)))
                    solution = np.argmin(responseAcc) + 1

            if get_image_pixel_similarity(ImageChops.logical_and(figA.convert('1'), figB.convert('1')).convert('RGB'),
                                          figC.convert('RGB')) > 0.95:
                if get_image_pixel_similarity(
                        ImageChops.logical_and(figD.convert('1'), figE.convert('1')).convert('RGB'),
                        figF.convert('RGB')) > 0.95:
                    generatedResponse = ImageChops.logical_and(figG.convert('1'), figH.convert('1'))
                    responseAcc = np.zeros((8, 1))
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        responseAcc[responseIdx - 1] = get_image_pixel_similarity(responseFig.convert('RGB'),
                                                                                  generatedResponse.convert('RGB'))
                    return np.argmax(responseAcc) + 1

            if get_image_pixel_similarity(ImageChops.logical_xor(figA.convert('1'), figB.convert('1')).convert('RGB'),
                                          figC.convert('RGB')) > 0.95:
                if get_image_pixel_similarity(
                        ImageChops.logical_xor(figD.convert('1'), figE.convert('1')).convert('RGB'),
                        figF.convert('RGB')) > 0.95:
                    generatedResponse = ImageChops.logical_xor(figG.convert('1'), figH.convert('1'))
                    responseAcc = np.zeros((8, 1))
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        responseAcc[responseIdx - 1] = get_image_pixel_similarity(responseFig.convert('RGB'),
                                                                                  generatedResponse.convert('RGB'))
                    return np.argmax(responseAcc) + 1

            if get_image_pixel_similarity(ImageChops.logical_or(figA.convert('1'), figB.convert('1')).convert('RGB'),
                                          figC.convert('RGB')) > 0.95:
                if get_image_pixel_similarity(
                        ImageChops.logical_or(figD.convert('1'), figE.convert('1')).convert('RGB'),
                        figF.convert('RGB')) > 0.95:
                    generatedResponse = ImageChops.logical_or(figG.convert('1'), figH.convert('1'))
                    responseAcc = np.zeros((8, 1))
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        responseAcc[responseIdx - 1] = get_image_pixel_similarity(responseFig.convert('RGB'),
                                                                                  generatedResponse.convert('RGB'))
                    return np.argmax(responseAcc) + 1

            if get_image_pixel_similarity(ImageOps.invert(ImageChops.difference(figA, figB)).convert('RGB'),
                                          figC.convert('RGB')) > 0.95:
                if get_image_pixel_similarity(ImageOps.invert(ImageChops.difference(figD, figE)).convert('RGB'),
                                              figF.convert('RGB')) > 0.95:
                    generatedResponse = ImageOps.invert(ImageChops.difference(figG, figH))
                    responseAcc = np.zeros((8, 1))
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        responseAcc[responseIdx - 1] = get_image_pixel_similarity(responseFig.convert('RGB'),
                                                                                  generatedResponse.convert('RGB'))
                    return np.argmax(responseAcc) + 1

            if get_image_pixel_similarity(ImageOps.invert(ImageChops.add(figA, figB)).convert('RGB'),
                                          figC.convert('RGB')) > 0.95:
                if get_image_pixel_similarity(ImageOps.invert(ImageChops.add(figD, figE)).convert('RGB'),
                                              figF.convert('RGB')) > 0.95:
                    generatedResponse = ImageOps.invert(ImageChops.add(figG, figH))
                    responseAcc = np.zeros((8, 1))
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        responseAcc[responseIdx - 1] = get_image_pixel_similarity(responseFig.convert('RGB'),
                                                                                  generatedResponse.convert('RGB'))
                    return np.argmax(responseAcc) + 1

            AxorBoffset = ImageChops.logical_xor(figA.convert('1'), ImageChops.offset(figB, -50, 0).convert('1'))
            AxorBoffset = ImageOps.invert(AxorBoffset.crop(AxorBoffset.getbbox()).convert('L'))
            DxorEoffset = ImageChops.logical_xor(figD.convert('1'), ImageChops.offset(figE, -50, 0).convert('1'))
            DxorEoffset = ImageOps.invert(DxorEoffset.crop(DxorEoffset.getbbox()).convert('L'))

            if get_image_pixel_similarity(AxorBoffset.convert('RGB'),
                                          figC.crop(ImageOps.invert(figC).getbbox()).convert('RGB')) > 0.95:
                if get_image_pixel_similarity(DxorEoffset.convert('RGB'),
                                              figF.crop(ImageOps.invert(figF).getbbox()).convert('RGB')) > 0.95:
                    generatedResponse = ImageChops.logical_xor(figG.convert('1'),
                                                               ImageChops.offset(figH, -50, 0).convert('1'))
                    generatedResponse = ImageOps.invert(
                        generatedResponse.crop(generatedResponse.getbbox()).convert('L'))
                    responseAcc = np.zeros((8, 1))
                    for responseIdx in range(1, 9):
                        responseFig = loadImage(problem, str(responseIdx))
                        responseAcc[responseIdx - 1] = get_image_pixel_similarity(generatedResponse.convert('RGB'),
                                                                                  responseFig.crop(ImageOps.invert(
                                                                                      responseFig).getbbox()).convert(
                                                                                      'RGB'))
                    return np.argmax(responseAcc) + 1

        return solution
